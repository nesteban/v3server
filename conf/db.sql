DROP table account;
DROP table customer;
DROP table project;
DROP table log;
DROP table logaction;
DROP table visit;

-- TABLE : ACCOUNT
CREATE TABLE IF NOT EXISTS account (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL CHECK (name <> ''),
  mail VARCHAR(50) NOT NULL CHECK (mail <> ''),
  password VARCHAR(50) NOT NULL CHECK (password <> '')
);
-- TABLE : CUSTOMER
CREATE TABLE IF NOT EXISTS customer (
  	id SERIAL PRIMARY KEY,
  	name VARCHAR(50) NOT NULL CHECK (name <> ''),
  	codename VARCHAR(5) NOT NULL CHECK (codename <> ''),
	passkey VARCHAR(8),
	addr1 VARCHAR(100),
	addr2 VARCHAR(100),
	city VARCHAR(20),
	citycode VARCHAR(10)
);
-- TABLE : PROJECT
CREATE TABLE IF NOT EXISTS project (
	id SERIAL PRIMARY KEY,
	idaddress BIGINT,
	name VARCHAR(50) NOT NULL CHECK (name <> ''),
	codename VARCHAR(5) NOT NULL CHECK (codename <> ''),
	idcustomer BIGINT NOT NULL,
	lock BOOLEAN NOT NULL
);
-- TABLE : VISIT
CREATE TABLE IF NOT EXISTS visit (
  	id SERIAL PRIMARY KEY,
  	idproject BIGINT NOT NULL,
  	idaddress BIGINT,
  	name VARCHAR(50) NOT NULL CHECK (name <> ''),
	reference VARCHAR(20) NOT NULL,
	meter float4,
	createdate TIMESTAMP NOT NULL,
	lock BOOLEAN NOT NULL
);
-- TABLE : LOG
CREATE TABLE IF NOT EXISTS log (
	datelog date,
	idvisit BIGINT,
	idproject BIGINT,
	idaction BIGINT,
	uuid VARCHAR(20),
	message VARCHAR(200)
);
-- TABLE : LOGACTION
CREATE TABLE IF NOT EXISTS logaction (
	id SERIAL PRIMARY KEY,
	name VARCHAR(50) NOT NULL CHECK (name <> '')
);  
-- TABLE : BILL
CREATE TABLE IF NOT EXISTS bill (
  	id SERIAL PRIMARY KEY,
  	reference VARCHAR(50),
  	idcustomer BIGINT NOT NULL,
  	billdate TIMESTAMP NOT NULL,
	meter INTEGER NOT NULL,
	total MONEY NOT NULL,
	path VARCHAR(200)
);
-- TABLE ADDRESS
CREATE TABLE IF NOT EXISTS address (
  	id SERIAL PRIMARY KEY,
  	addr1 VARCHAR(100),
	addr2 VARCHAR(100),
	city VARCHAR(20),
	citycode VARCHAR(10),
	geolocation POINT --postgis type
);


INSERT INTO customer(name,codename,passkey,addr1,addr2,city,citycode)
	VALUES('Promoteur 1','prom1','','3, rue du Mar��chal Foch','','33000','Bordeaux');
	
INSERT INTO project(name,codename,idCustomer,lock)
	VALUES('R��sidence Le CLos','leclo',1,false);
	
INSERT INTO visit(idproject,name,reference,meter,createdate,lock)
    VALUES(1,'T2 - 2i��me ��tage','1234',60,'20131010',false);

INSERT INTO logaction(name) values ('DOWNLOAD');
INSERT INTO logaction(name) values ('ENTER VISIT');
INSERT INTO log(datelog,idaction,idvisit,idproject,uuid,message)
    VALUES('20131010',1,1,1,'uuid-32','no msg');
INSERT INTO account(name,mail,password) VALUES ('Nicolas Esteban','n.esteban@aldera.fr','ne');
  