import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "v3srv"
  val appVersion      = "0.1.0"


  
  
  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "com.typesafe" %% "play-plugins-mailer" % "2.1.0",
    "com.typesafe.play" %% "play-slick" % "0.5.0.8",
    "com.github.tminglei" % "slick-pg_2.10.1" % "0.2.2",
    "org.postgresql" % "postgresql" % "9.2-1002-jdbc4",
    "com.github.tototoshi" %% "slick-joda-mapper" % "0.4.0"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
      resolvers += "Sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"
  )

}