package models

import java.sql.Date
import play.api.Play.current
//import play.api.db.slick.Config.driver.simple._
import MyPostgresDriver.simple._
import slick.lifted.{ Join, MappedTypeMapper }
import play.api.db.slick.DB
import play.api.Logger


case class CustomerModel(id: Option[Long] = None, name: String, codename: String, passkey: Option[String], addr1: Option[String], addr2: Option[String], city: Option[String], citycode: Option[String])

object Customers extends Table[CustomerModel]("customer") {

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.NotNull)
    def codename = column[String]("codename", O.NotNull)
    def passkey = column[Option[String]]("passkey")
    def addr1 = column[Option[String]]("addr1")
    def addr2 = column[Option[String]]("addr2")
    def city = column[Option[String]]("city")
    def citycode = column[Option[String]]("citycode")
    def * = id ~ name ~ codename ~ passkey ~ addr1 ~ addr2 ~ city ~ citycode  <> (CustomerModel.apply _, CustomerModel.unapply _)

	def autoInc = name ~ codename ~ passkey ~ addr1 ~ addr2 ~ city ~ citycode returning id
	
	def add(c: CustomerModel) = DB.withSession  { implicit session: Session => 
     	autoInc.insert(c.name, c.codename, c.passkey, c.addr1, c.addr2, c.city, c.citycode) 
   	}
  def count(filter: String)(implicit s: Session): Int = Query(Customers.where(_.name.toLowerCase like filter.toLowerCase).length).first
  def list(page: Int=0, pageSize: Int=10, filter: String = "%"):Page[CustomerModel] = DB.withSession { implicit session: Session =>
    val offset = pageSize * page
    val query = (for { 
            c <- Customers
          } yield (c))
            .drop(offset)
            .take(pageSize)
    val totalRows = count(filter)
    val result = query.list.sortBy(r => r.name)//.map(row => (row._1))
    Page(result, page, offset, totalRows)
  }
  def listAll: List[CustomerModel] = DB.withSession { implicit session: Session =>
    return Query(Customers).list.sortBy(r => r.name)
  }
  def findById(id: Long): Option[CustomerModel] = DB.withSession  { implicit session: Session => 
      return Query(Customers).where(r => r.id === id).firstOption
    }

  def update(id: Long, customer: CustomerModel) = DB.withSession  {implicit session: Session =>
    val customerToUpdate: CustomerModel = customer.copy(Some(id))
    Customers.where(_.id === id).update(customerToUpdate)
  }

  def delete(id: Long) = DB.withSession { implicit session: Session =>
    // Manual Cascad delete

    // Projects
    val queryProject = for { p <- Projects if p.idcustomer === id } yield(p)
    val mapPrj = queryProject.map(row => (row.id)).list

    // Visits
    val queryVisit = for {
      v <- Visits if mapPrj.contains(v.id)
    } yield(v)

    // Customers
    val queryCustomer = for { c <- Customers if c.id === id } yield (c)
    queryVisit.delete
    queryProject.delete
    queryCustomer.delete
  }
  def searchByPattern(pattern: String): List[CustomerModel] = DB.withSession  {implicit session: Session =>
        val q = for {
          c <- Customers if ((c.name.toLowerCase like pattern.toLowerCase) 
                            || (c.codename.toLowerCase like pattern.toLowerCase)
                            || (c.addr1.toLowerCase like pattern.toLowerCase)
                            || (c.addr2.toLowerCase like pattern.toLowerCase)
                            || (c.city.toLowerCase like pattern.toLowerCase)
                            || (c.citycode.toLowerCase like pattern.toLowerCase))
        } yield(c)
        return q.list
    }
}
