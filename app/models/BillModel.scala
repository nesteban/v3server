package models

import java.sql.Date
import play.api.Play.current
import MyPostgresDriver.simple._
import slick.lifted.{ Join, MappedTypeMapper }
import play.api.db.slick.DB
import play.api.Logger
import java.sql.Timestamp

case class BillModel(id: Option[Long] = None, reference: Option[String], idcustomer: Long, billdate: Timestamp, meter: Int, total: Double, path: Option[String])

object Bills extends Table[BillModel]("bill") {

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def reference = column[Option[String]]("reference")
    def idcustomer = column[Long]("idcustomer", O.NotNull)
    def billdate = column[Timestamp]("billdate",O.NotNull)
    def meter = column[Int]("meter",O.NotNull)
    def total = column[Double]("total",O.NotNull)
    def path = column[Option[String]]("path")
    def * = id ~ reference ~ idcustomer ~ billdate ~ meter ~ total ~ path  <> (BillModel.apply _, BillModel.unapply _)

	def autoInc = reference ~ idcustomer ~ billdate ~ meter ~ total ~ path returning id
	
	def add(b: BillModel): Option[Long] = DB.withSession  { implicit session: Session => 
        return autoInc.insert(b.reference, b.idcustomer, b.billdate, b.meter, b.total, b.path) 
    }
    /**
     * 
     */
	def listByCustomer(idcustomer: Long): List[BillModel] = DB.withSession { implicit session: Session =>
		return Query(Bills).where(b => b.idcustomer === idcustomer).list
	}
}
