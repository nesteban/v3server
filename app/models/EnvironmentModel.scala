package models

import java.sql.Date
import play.api.Play.current
//import play.api.db.slick.Config.driver.simple._
import MyPostgresDriver.simple._
import slick.lifted.{ Join, MappedTypeMapper }
import play.api.db.slick.DB
import play.api.Logger
import play.api.Play.current

case class EnvironmentModel(id: Option[Long] = None, name: String, suffix: String)

object Environments {

  val ANDROID = "_android.unity3d";
  val IOS = "_ios.unity3d";
  val MAC = "_mac.unity3d";
  val WIN = "_win.unity3d";


}
