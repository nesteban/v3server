 package models
 
import java.sql.Date
import play.api.Play.current
//import play.api.db.slick.Config.driver.simple._
import MyPostgresDriver.simple._
import play.api.db.slick.DB

case class LogModel(datelog: java.sql.Date, idvisit: Long, idproject: Long, idaction: Long, uuid: String, message: String)
case class LogActionModel(id: Option[Long], name: String)

object LogActions extends Table[LogActionModel]("logaction") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.NotNull)
    def * = id ~ name <> (LogActionModel.apply _, LogActionModel.unapply _)
}
object Logs extends Table[LogModel]("log") {
    def datelog = column[java.sql.Date]("datelog")
    def idvisit = column[Long]("idproject")
    def idproject = column[Long]("idproject")
    def idaction = column[Long]("idaction")
    def uuid = column[String]("uuid")
    def message = column[String]("message")
    def * = datelog ~ idvisit ~ idproject ~ idaction ~ uuid ~ message <> (LogModel.apply _, LogModel.unapply _)

    def count(filter: String)(implicit s: Session): Int = Query(Logs.where(_.message.toLowerCase like filter.toLowerCase).length).first
    
    def list(page: Int=0, pageSize: Int=10, filter: String = "%"):Page[(LogModel,LogActionModel,VisitModel,ProjectModel,CustomerModel)] = DB.withSession { implicit session: Session =>
        val offset = pageSize * page
        val query = (for {
            l <- Logs 
            a <- LogActions if l.idaction === a.id
            v <- Visits if l.idvisit === v.id
            p <- Projects if l.idproject === p.id
            c <- Customers if p.idcustomer === c.id
          } yield (l, a, v, p, c))
                .drop(offset)
                .take(pageSize)
        val totalRows = count(filter)
        val result = query.list.map(row => (row._1, row._2, row._3, row._4, row._5))
        Page(result, page, offset, totalRows)

    }
}