package models

import java.sql.Date
import play.api.Play.current
//import play.api.db.slick.Config.driver.simple._
import MyPostgresDriver.simple._
import play.api.db.slick.DB
import play.api.Logger

case class ProjectModel(id: Option[Long] = None, name: String, codename: String, idcustomer: Long, idaddress: Option[Long], lock: Option[Boolean])

object Projects extends Table[ProjectModel]("project") {

  def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name", O.NotNull)
  def codename = column[String]("codename", O.NotNull)
  def idcustomer = column[Long]("idcustomer", O.NotNull)
  def idaddress = column[Option[Long]]("idaddress")
  def lock = column[Option[Boolean]]("lock")
  def * = id ~ name ~ codename ~ idcustomer ~ idaddress ~ lock <> (ProjectModel.apply _, ProjectModel.unapply _)
  def autoInc = name ~ codename ~ idcustomer ~ idaddress ~ lock returning id
	
	def add(c: ProjectModel) = DB.withSession  { implicit session: Session => 
      val lockValue = c.lock.getOrElse(false)
     	autoInc.insert(c.name, c.codename, c.idcustomer, c.idaddress, Some(lockValue)) 
   	}

  def findById(id: Long): Option[ProjectModel] = DB.withSession  { implicit session: Session => 
      return Query(Projects).where(r => r.id === id).firstOption
    }
  def findByCustomer(idcustomer: Long): List[ProjectModel] = DB.withSession { implicit session: Session =>
        return (for { p <- Projects if p.idcustomer === idcustomer } yield(p)).list.sortBy(r => r.id)
  }
  def update(id: Long, idaddress: Option[Long], project: ProjectModel) = DB.withSession  {implicit session: Session =>
    val projectToUpdate: ProjectModel = project.copy(Some(id),project.name, project.codename, project.idcustomer, idaddress, Some(project.lock.getOrElse(false)))
    Projects.where(_.id === id).update(projectToUpdate)
  }
  def count(filter: String)(implicit s: Session): Int = Query(Projects.where(_.name.toLowerCase like filter.toLowerCase).length).first
  def list(page: Int=0, pageSize: Int=10, filter: String = "%"):Page[(ProjectModel,CustomerModel)] = DB.withSession { implicit session: Session =>
    val offset = pageSize * page
    val query = (for {
          p <- Projects
          c <- Customers if p.idcustomer === c.id
        } yield (p, c))
            .drop(offset)
            .take(pageSize)
    val totalRows = count(filter)
    val result = query.list.map(row => (row._1, row._2))
    Page(result, page, offset, totalRows)

  }

  // Return 
  def listByCustomers = DB.withSession { implicit session: Session =>
    val query = (for {
      p <- Projects
      c <- Customers if p.idcustomer === c.id
    } yield (p.id, p.name, c.name)).list.groupBy(_._3)

    query.map {
      case(cusname, pbc) => (cusname, pbc.map(r2 => (r2._1, r2._2, r2._3)))
    }
  }

    def delete(id: Long) = DB.withSession { implicit session: Session =>
    // Manual Cascad delete
    val queryProject = for { p <- Projects if p.id === id } yield(p)
    val queryVisit = for { v <- Visits if v.idproject === id } yield(v)

    queryVisit.delete
    queryProject.delete
  }
  def searchByPattern(pattern: String): List[ProjectModel] = DB.withSession  {implicit session: Session =>
    val q = for {
      p <- Projects if ((p.name.toLowerCase like pattern.toLowerCase)
                            || (p.codename.toLowerCase like pattern.toLowerCase))
    }yield(p)
    return q.list
  }
}
