 package models
 
import java.sql.Timestamp
import play.api.Play.current
//import play.api.db.slick.Config.driver.simple._
import MyPostgresDriver.simple._
import play.api.data.format.Formats._
import play.api.db.slick.DB
import play.api.Logger


case class VisitModel(id: Option[Long], idproject: Long, idaddress: Option[Long], name: String, reference: String, meter: Option[Float], createdate: Option[Timestamp], lock: Option[Boolean])

object Visits extends Table[VisitModel]("visit") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def idproject = column[Long]("idproject", O.NotNull)
    def idaddress = column[Option[Long]]("idaddress")
    def name = column[String]("name", O.NotNull)
    def reference = column[String]("reference", O.NotNull)
	def meter = column[Option[Float]]("meter")
    def createdate = column[Option[Timestamp]]("createdate")
    def lock = column[Option[Boolean]]("lock", O.NotNull)
    def * = id ~ idproject ~ idaddress ~ name ~ reference ~ meter ~ createdate ~ lock <> (VisitModel.apply _, VisitModel.unapply _)

    def autoInc = idproject ~ idaddress ~ name ~ reference ~ meter ~ createdate ~ lock returning id
  
    def findById(id: Long): Option[VisitModel] = DB.withSession  { implicit session: Session => 
      return Query(Visits).where(r => r.id === id).firstOption
    }    
    def findByReference(reference: String) = DB.withSession  { implicit session: Session => 
      Query(Visits).where(r => r.reference === reference)
    }
    def findByProject(idproject: Long): List[VisitModel] = DB.withSession { implicit session: Session =>
        return (for { v <- Visits if v.idproject === idproject } yield(v)).list.sortBy(r => r.id)
    }
    def count: Int = DB.withSession { implicit session: Session => Query(Visits.length).first }
    def count(filter: String)(implicit s: Session): Int = Query(Visits.where(_.name.toLowerCase like filter.toLowerCase).length).first
    
    def list(page: Int=0, pageSize: Int=10, filter: String = "%"):Page[(VisitModel,ProjectModel,CustomerModel)] = DB.withSession { implicit session: Session =>
        val offset = pageSize * page
        val query = (for {
            v <- Visits
            p <- Projects if v.idproject === p.id
            c <- Customers if p.idcustomer === c.id
          } yield (v, p, c))
            .drop(offset)
            .take(pageSize)
        val totalRows = count(filter)
        val result = query.list.map(row => (row._1, row._2, row._3))
        Page(result, page, offset, totalRows)
    }

    def add(v: VisitModel) = DB.withSession  { implicit session: Session => 
        val lockValue = v.lock.getOrElse(false)
        var id = autoInc.insert(v.idproject,v.idaddress, v.name, v.reference, v.meter, v.createdate, Some(lockValue)) 

        // Update the reference
        // Get codenames from Customer and Project
        val qnames = for { 
            v <- Visits if v.id === id
            p <- Projects if p.id === v.idproject
            c <- Customers if  p.idcustomer === c.id } yield ( c.codename, p.codename )
        val codenames = qnames.list.map(r => r._1 + r._2).head // concat 2 codes names
        val q = for { v <- Visits if v.id === id} yield (v.reference)    // select row to update
        q.update("" + codenames + id.get) // concat codenames + id and update
    }
    def update(id: Long, createDate: Option[Timestamp], idaddress: Option[Long], visit: VisitModel) = DB.withSession  {implicit session: Session =>
        val visitToUpdate: VisitModel = visit.copy(Some(id),visit.idproject, idaddress,visit.name, visit.reference, Some(visit.meter.getOrElse(0)), createDate, Some(visit.lock.getOrElse(false)))
        Visits.where(_.id === id).update(visitToUpdate)
    }
    def delete(id: Long) = DB.withSession { implicit session: Session =>
        val queryVisit = for { v <- Visits if v.id === id } yield(v)
        queryVisit.delete
    }
    def searchByPattern(pattern: String): List[VisitModel] = DB.withSession  {implicit session: Session =>
        val q = for {
          v <- Visits if ((v.name.toLowerCase like pattern.toLowerCase)
                            || (v.reference.toLowerCase like pattern.toLowerCase))
        }yield(v)
        return q.list
    }
}