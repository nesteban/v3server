package models

import java.sql.Date
import play.api.Play.current
//import play.api.db.slick.Config.driver.simple._
import MyPostgresDriver.simple._
import slick.lifted.{ Join, MappedTypeMapper }
import play.api.db.slick.DB
import play.api.Logger


case class AccountModel(id: Option[Long] = None, name: String, mail: String, password: String)

object Accounts extends Table[AccountModel]("account") {

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.NotNull)
    def mail = column[String]("mail", O.NotNull)
    def password = column[String]("password", O.NotNull)
    def * = id ~ name ~ mail ~ password  <> (AccountModel.apply _, AccountModel.unapply _)

	def autoInc = name ~ mail ~ password returning id
	
	def add(a: AccountModel) = DB.withSession  { implicit session: Session => 
     	autoInc.insert(a.name, a.mail, a.password) 
   	}

  def findByMail(mail: String): Option[AccountModel] = DB.withSession { implicit session: Session => 
      return Query(Accounts).where(r => r.mail === mail).firstOption
  }

  def update(id: Long, account: AccountModel) = DB.withSession  {implicit session: Session =>
    val accountToUpdate: AccountModel = account.copy(Some(id))
    Accounts.where(_.id === id).update(accountToUpdate)
  }

  def delete(id: Long) = DB.withSession { implicit session: Session =>
    val accountToDelete = for { a <- Accounts if a.id === id } yield(a)
    accountToDelete.delete
  }

  def authenticate(mail: String, password: String): Boolean = DB.withSession { implicit session: Session =>
    val account = for { a <- Accounts if a.mail === mail && a.password === password } yield(a)
    return (account.list.length == 1)
  }
}
