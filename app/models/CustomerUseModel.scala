package models

import java.sql.Date
import play.api.Play.current
import MyPostgresDriver.simple._
import slick.lifted.{ Join, MappedTypeMapper }
import play.api.db.slick.DB
import play.api.Logger
import java.sql.Timestamp
import scala.slick.ast.SortBy

case class CustomerUseModel(id: Option[Long] = None, idcustomer: Long, idbill: Option[Long], idvisit: Option[Long], usedate: Timestamp, meter: Int, credit: Int)

object CustomerUses extends Table[CustomerUseModel]("customeruse") {

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def idcustomer = column[Long]("idcustomer", O.NotNull)
    def idbill = column[Option[Long]]("idbill")
    def idvisit = column[Option[Long]]("idbill")
    def usedate = column[Timestamp]("billdate",O.NotNull)
    def meter = column[Int]("meter",O.NotNull)
    def credit = column[Int]("total",O.NotNull)
    def * = id ~ idcustomer ~ idbill ~ idvisit ~ usedate ~ meter ~ credit <> (CustomerUseModel.apply _, CustomerUseModel.unapply _)

	def autoInc = idcustomer ~ idbill ~ idvisit ~ usedate ~ meter ~ credit returning id
	
	def addBill(b: BillModel,credit: Int) = DB.withSession  { implicit session: Session => 
        autoInc.insert(b.idcustomer, b.id, None, b.billdate, b.meter, credit) 
    }
    def addVisit(v: VisitModel, idcustomer: Long, credit: Int) = DB.withSession  { implicit session: Session => 
        autoInc.insert(	idcustomer,None,v.id,v.createdate.get,v.meter.get.toInt,credit) 
    }
    def getCredit(idcustomer: Long): Int = DB.withSession  { implicit session: Session => 
    	val q = (for {
    	  c <- CustomerUses if c.idcustomer === idcustomer
    	}yield(c.credit, c.usedate)).sortBy(_._2)
    	q.map(r => r._1).list.head
    }
}
