package models
 
case class SearchResults(visits: List[VisitModel], projects: List[ProjectModel], customers: List[CustomerModel])

