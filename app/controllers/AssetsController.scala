package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.Play.current
import play.api.libs.iteratee._
import play.api.libs.concurrent.Execution.Implicits._

import play.api.Logger

object AssetsController extends Controller {

	def download(environment: String, reference: String) = Action {

		val fullpath = Play.current.configuration.getString("assets_dir").getOrElse("public/www/") + reference + "_" + environment + ".unity3d" 
		Logger.debug("download  " + fullpath)

	  	val file = new java.io.File(fullpath)
	  	if(file.exists)
	  	{
	  		  	val fileContent: Enumerator[Array[Byte]] = Enumerator.fromFile(file)    
	  		    
	  			  SimpleResult(
	  			    header = ResponseHeader(200, Map(CONTENT_LENGTH -> file.length.toString)),
	  			    body = fileContent
	  			  )
	  	}
	  	else
	  		NotFound
	}
	def downloadPreview(reference: String) = Action {

		val fullpath = Play.current.configuration.getString("assets_dir").getOrElse("public/www/") + reference + "_preview.png" 
		Logger.debug("download  " + fullpath)

	  	val file = new java.io.File(fullpath)
	  	if(file.exists)
	  	{
	  	val fileContent: Enumerator[Array[Byte]] = Enumerator.fromFile(file)    
	    
		  SimpleResult(
		    header = ResponseHeader(200, Map(CONTENT_LENGTH -> file.length.toString)),
		    body = fileContent
		    )
		}
		else
			NotFound
	}
	def preview(reference: String) = Action {
		val fullpath = Play.current.configuration.getString("assets_dir").getOrElse("public/www/") + reference + "_preview.png"
		val file = new java.io.File(fullpath)
		if (file.exists)
		{
    		Ok.sendFile(file)
    	}
    	else 
    		NotFound
 	}
}