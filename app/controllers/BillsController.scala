package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.Play.current
import org.joda.time.LocalDateTime
import models._
import java.sql.Timestamp
import org.joda.time._



object BillsController extends Controller with Secured {
  
  val accountForm = Form {
    tuple(
      "email" -> text,
      "password" -> text
    ) verifying("Invalid email or password", result => result match {
      case (mail, password) => Accounts.authenticate(mail, password)
    })
  }
  
  
  val billForm = Form {
		tuple(
			"meter" -> number,
			"total" -> number
		)
  }
	
  def view(idcustomer: Long) = IsAuthenticated { implicit request =>
    val c = Customers.findById(idcustomer)
    if(c.isDefined)
    	Ok(views.html.customer.billscustomer(c.get,Bills.listByCustomer(idcustomer)))
    else
      NotFound
  }
  
	// POST
  	def purchase(idcustomer: Long) = IsAuthenticated { implicit request =>
  		
  		billForm.bindFromRequest.fold( 
  				hasErrors = { form =>  
  					Logger.debug("form=" + form.data); 
  					Redirect(routes.BillsController.view(idcustomer)).flashing(Flash(form.data) + ("error" -> "Erreur de saisie. V��rifiez que tous les champs obligatoires sont saisis.") )
  				},
  				success = { newBillValues =>
  				  	val dt = new DateTime
  				  	val ts = new Timestamp(dt.getMillis())
		        	val id = Bills.add(BillModel(None,None,idcustomer,ts,newBillValues._1,newBillValues._2,None))
		        	// Compute credit for customer
		        	
		        	CustomerUses.addBill(BillModel(id,None,idcustomer,ts,newBillValues._1,newBillValues._2,None), getCredits(id.get) + newBillValues._1)
		          	val message = "Achat confirmé !"
		          	Redirect(routes.CustomersController.view(idcustomer)).flashing("success" -> message)
		        }
  		)
  	}
  	def getCredits(customerid: Long): Int  =  {
  	  return 0
  	}
}