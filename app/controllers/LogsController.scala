package controllers

import play.api._
import play.api.mvc._

import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.data._
import play.api.Play.current
import play.api.data.Forms._
import play.api.data.format.Formats._
import java.util.Date
import java.sql.Date
import models._
import play.api.Logger


object LogsController extends Controller with Secured{

	/* ACTION LOG CONSTANTES
	val ACTION_DOWNLOADVISIT : Long = 1
	val ACTION_ENTERVISIT : Long = 2
	*/
	
	def list(page: Int=0, pageSize: Int=10) = IsAuthenticated { implicit request =>
		Ok(views.html.logs.list(Logs.list(page,pageSize),pageSize))
	}

	val logForm = Form(
		mapping(
		  "datelog" -> of[java.sql.Date],
		  "idvisit" -> longNumber,
		  "idcustomer" -> longNumber,
		  "idaction" -> longNumber,
		  "uuid" -> text,
		  "message" -> text
		)(LogModel.apply)(LogModel.unapply)
	)

	def add = DBAction  { implicit request =>

		logForm.bindFromRequest.fold(
        	hasErrors = { form =>  
            	Ok(views.html.logs.fail())
        	},
        	success = { newLog =>
        		Logs.insert(newLog)
          		Ok(views.html.logs.success())
        	}
      	)
		/*
		val currentDate = new java.util.Date
		val sqlDate =new java.sql.Date(currentDate.getTime())

		Ok(views.html.logs.success)
		*/
		//Logs.insert(sqlDate, idvisit,idproject,ACTION_ENTERVISIT,uuid,message)
		//(Logs.datelog ~ Logs.idvisit ~ Logs.idproject ~ Logs.idaction ~ Logs.uuid ~ Logs.message).insert(new java.sql.Date(currentDate.getTime()),idvisit,idproject,ACTION_ENTERVISIT,uuid,message)
	} 
}