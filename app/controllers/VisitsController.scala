package controllers

import play.api._
import play.api.mvc._
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.Play.current
import models._
import play.api.Logger
import java.sql.Date
import play.api.libs.json.Json

object VisitsController extends Controller with Secured {

  def referer(implicit request: Request[AnyContent]) =
    request.headers.get(REFERER).getOrElse("/visits")

	def list(page: Int=0, pageSize: Int=10) = IsAuthenticated { implicit request =>
		val visits = Visits.list(page,pageSize)
    // Check all environments available and remap the visits
    val result = visits.items.map(row => (row._1, row._2, row._3, environmentAvailable(row._1.reference)))
		Ok(views.html.visit.listvisits(Page(result,visits.page,visits.offset,visits.total),pageSize))
	}
  def view(id: Long) = IsAuthenticated { implicit request =>
    val v = Visits.findById(id)
    val p = Projects.findById(v.get.idproject)
    val c = Customers.findById(p.get.idcustomer)
    Ok(views.html.visit.visit(v.get,p.get,c.get,environmentAvailable(v.get.reference)))
  }

	val visitForm = Form(
		mapping(
			"id" -> optional(longNumber),
			"idproject" -> longNumber,
			"idaddress" ->  ignored(Option.empty[Long]),
			"name" -> text,
			"reference" -> text,
			"meter" -> optional(of[Float]),
			"createdate" ->  ignored(Option.empty[java.sql.Timestamp]),
      		"lock" -> optional(boolean)
		)(VisitModel.apply)(VisitModel.unapply)
	)

	def newVisit = IsAuthenticated { implicit request =>
		val form: Form[models.VisitModel] = if(flash.get("error").isDefined)
		{
			this.visitForm.bind(flash.data)
		}
		else
		{
			this.visitForm 
		}
		val projectsByCustomers = Projects.listByCustomers
		Ok(views.html.visit.addvisit(form, projectsByCustomers))
	}

	// POST
  	def save = IsAuthenticated { implicit request =>
      visitForm.bindFromRequest.fold(
        hasErrors = { form =>  
            Logger.debug("form=" + form.data); 
            Redirect(routes.VisitsController.newVisit).flashing(Flash(form.data) + ("error" -> "Erreur de saisie. V��rifiez que tous les champs obligatoires sont saisis.") )
        },
        success = { newVisit =>
        	val dt = new org.joda.time.DateTime
        	val ts = new java.sql.Timestamp(dt.getMillis())
          	Visits.add(VisitModel(None,newVisit.idproject, None, newVisit.name, newVisit.reference, newVisit.meter,Some(ts),newVisit.lock))

          	val message = "Ajout du client"
          	Redirect(routes.VisitsController.list(0,10)).flashing("success" -> message)
        }
      )
    }

	def edit(id: Long) = IsAuthenticated { implicit request =>
		val v = Visits.findById(id)
		val projectsByCustomers = Projects.listByCustomers
    val from = request.queryString.get("from").flatMap(_.headOption)

		Ok(views.html.visit.editvisit(v.get,projectsByCustomers,from))
	}

  // POST
  def update(id: Long, from: String="/visits") = IsAuthenticated { implicit request =>

    val newvisitForm = this.visitForm.bindFromRequest()
    newvisitForm.fold(
      hasErrors = { form =>
      	Logger.debug("error:" + form.data)
        Redirect(routes.VisitsController.edit(id)).
        flashing(Flash(form.data) +
        ("error" -> "Erreur de saisie. V��rifiez que tous les champs obligatoires sont saisis.") )
      },
      success = { newVisit =>
        val v = Visits.findById(id)
        models.Visits.update(id, v.get.createdate, v.get.idaddress, newVisit)
        val message = "Visit Added"
        Redirect(from).flashing("success" -> message)
      }
    )
  }
  def delete(id: Long) = IsAuthenticated { implicit request =>
    Visits.delete(id)
    Redirect(routes.VisitsController.list(0,10))
  }

  def environmentAvailable(reference: String) : List[Boolean] = {
    val path = Play.current.configuration.getString("assets_dir").getOrElse("public/www/") + reference
    val androidFile = new java.io.File(path + Environments.ANDROID)
    val iosFile = new java.io.File(path + Environments.IOS)
    val macFile = new java.io.File(path + Environments.MAC)
    val winFile = new java.io.File(path + Environments.WIN)
    val envs : List[Boolean] = List (
        androidFile.exists,
        iosFile.exists,
        macFile.exists,
        winFile.exists
      )
    return envs;
  }
  //
  // PUBLIC - CLIENT
  //
  def details(reference: String) = DBAction { implicit request =>
    val v = Visits.findByReference(reference)
    if(v.list.length == 1)
    {
      val p = Projects.findById(v.first.idproject)
      var projectname = "unknown"
      if(p.isDefined) {
        projectname = p.get.name
      }
      val js = Json.obj( "name" -> v.first.name, "reference" -> v.first.reference, "img" -> (reference + "_preview.png"), "projectname" -> projectname)
      Ok(js)
    }
    else
    {
      NotFound("no visit found")
    } 
  }
}