package controllers

import play.api._
import play.api.mvc._
import models._

object Application extends Controller with Secured {
  
  def nav(section: String) =  {
	  views.html.nav(section)
  }

  def search(pattern: String) = IsAuthenticated { implicit request =>
  	val p = "%" + pattern + "%"
  	Ok(views.html.search(SearchResults(Visits.searchByPattern(p),Projects.searchByPattern(p),Customers.searchByPattern(p)), pattern))

  }
}