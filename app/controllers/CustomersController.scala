package controllers

import play.api._
import play.api.mvc._

import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.data._
import play.api.data.Forms._
import play.api.data.Forms.{mapping, nonEmptyText, text, ignored}
import play.api.Play.current
import models._
import play.api.Logger

object CustomersController extends Controller with Secured{

	def list(page: Int=0, pageSize: Int=10) = IsAuthenticated { implicit rs =>
		Ok(views.html.customer.listcustomers(Customers.list(page,pageSize),pageSize))
	}
  def view(id: Long) = IsAuthenticated { implicit request =>
    val c = Customers.findById(id)
    Ok(views.html.customer.customer(c.get,Projects.findByCustomer(id)))
  }
	val customerForm = Form(
		mapping(
			"id" -> optional(longNumber),
      "name" -> nonEmptyText,
			"codename" -> nonEmptyText,
			"passkey" -> optional(text),
			"addr1" -> optional(text),
			"addr2" -> optional(text),
			"city" ->  optional(text),
			"citycode" -> optional(text)
		)(CustomerModel.apply)(CustomerModel.unapply)
	)

	def newCustomer = IsAuthenticated { implicit rs =>
		val form: Form[models.CustomerModel] = if(flash.get("error").isDefined)
		{
			this.customerForm.bind(flash.data)
		}
		else
		{
			this.customerForm
		}
		Ok(views.html.customer.addcustomer(form))
	}
	def edit(id: Long) = IsAuthenticated { implicit request =>
    val user = Customers.findById(id)
    val from = request.queryString.get("from").flatMap(_.headOption)
    Ok(views.html.customer.editcustomer(user.get,from))
  }

  	// POST
	def save = IsAuthenticated { implicit rs =>
    	customerForm.bindFromRequest.fold(
        hasErrors = { form =>
            Redirect(routes.CustomersController.newCustomer()).flashing(Flash(form.data) + ("error" -> "Erreur de saisie. Vérifiez que tous les champs obligatoires sont saisis.") )
        },
        success = { newCustomer =>

          Customers.add(newCustomer)

          val message = "Ajout du client"
          Redirect(routes.CustomersController.list(0,10)).flashing("success" -> message)
        }
      )
  	}
  	// POST
  	def update(id: Long, from: String) = IsAuthenticated { implicit rs =>
      val newCustomerForm = this.customerForm.bindFromRequest()
      newCustomerForm.fold(
        hasErrors = { form =>
          Redirect(routes.CustomersController.edit(id)).
            flashing(Flash(form.data) +
            ("error" -> "Erreur de saisie. Vérifiez que tous les champs obligatoires sont saisis.") )
        },
        success = { newCustomer =>
        	Logger.debug("success " + id)
          
          Customers.update(id, newCustomer)

          val message = "Ajout du client"
          Redirect(from).flashing("success" -> message)
        }
      )
    }
    def delete(id: Long) = IsAuthenticated { implicit rs =>
      Customers.delete(id)
      Redirect(routes.CustomersController.list(0,10))
    }
}