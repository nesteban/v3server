package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.data._
import play.api.data.Forms._
import views.html.defaultpages._
import models._
import views._

object Auth extends Controller {


  val accountForm = Form {
    tuple(
      "email" -> text,
      "password" -> text
    ) verifying("Invalid email or password", result => result match {
      case (mail, password) => Accounts.authenticate(mail, password)
    })
  }

  def login = Action { implicit request =>
    Ok(html.login(accountForm))
  }

  def authenticate = Action { implicit request =>
    accountForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.login(formWithErrors)),
      user => Redirect(routes.DashboardController.index).withSession("mail" -> user._1)
    )
  }

  def logout = Action {
    Redirect(routes.Auth.login).withNewSession.flashing(
      "success" -> "You are now logged out."
    )
  }
}

case class AuthenticatedRequest[A](
                                    val user: AccountModel, request: Request[A]
                                    ) extends WrappedRequest(request)

/**
 * Provide security features
 */
trait Secured {

  /**
   * Retrieve the connected user email.
   */
  private def username(request: RequestHeader) = request.session.get("mail")

  /**
   * Redirect to login if the user in not authorized.
   */
  private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Auth.login)

  // --
    /*
  /**
   * Action for authenticated users.
   */
  def IsAuthenticated(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(request => f(user)(request))
  }  */
  def Authenticated[A](p: BodyParser[A])(f: AuthenticatedRequest[A] => Result) = {
    Action(p) { request =>
      request.session.get("mail").flatMap(u => Accounts.findByMail(u)).map { user =>
        f(AuthenticatedRequest(user, request))
      }.getOrElse(Results.Redirect(routes.Auth.login))
    }
  }

  // Overloaded method to use the default body parser
  import play.api.mvc.BodyParsers._
  def IsAuthenticated(f: AuthenticatedRequest[AnyContent] => Result): Action[AnyContent]  = {
    Authenticated(parse.anyContent)(f)
  }
}
