package controllers

import play.api._
import play.api.mvc._
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import models._

object Statistic extends Controller with Secured{

  def index = IsAuthenticated { implicit request =>
    Ok(views.html.statistic("Your new application is ready."))
  }



  def countVisits = IsAuthenticated { implicit request =>
    Ok(Visits.count.toString)
  }
}