package controllers

import play.api._
import play.api.mvc._
import models._

object DashboardController extends Controller with Secured {

  def index = IsAuthenticated { implicit request =>
    Ok(views.html.dashboard("Your new application is ready."))
  }

}