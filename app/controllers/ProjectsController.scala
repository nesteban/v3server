package controllers

import play.api._
import play.api.mvc._

import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.Play.current
import models._
import play.api.Logger

object ProjectsController extends Controller with Secured {


  def list(page: Int=0, pageSize: Int=10) = IsAuthenticated { implicit request => 
    Ok(views.html.project.listprojects(Projects.list(page,pageSize), pageSize))
  }
  def view(id: Long) = IsAuthenticated { implicit request =>
    val p = Projects.findById(id)
    val c = Customers.findById(p.get.idcustomer)
    Ok(views.html.project.project(p.get,c.get,Visits.findByProject(id)))
  }
  val projectForm = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "codename" -> nonEmptyText,
      "idcustomer" -> longNumber,
      "idaddress" -> ignored(Option.empty[Long]),
      "lock" -> optional(boolean)
    )(ProjectModel.apply)(ProjectModel.unapply)
  )

  def newProject = IsAuthenticated { implicit request =>
    val form: Form[ProjectModel] = if(flash.get("error").isDefined)
    {
      this.projectForm.bind(flash.data)
    }
    else
    {
      this.projectForm
    }
    val customers = Customers.listAll
    Ok(views.html.project.addproject(form,customers))
  }
    // POST
  def save = IsAuthenticated { implicit request =>
      projectForm.bindFromRequest.fold(
        hasErrors = { form =>  
            Logger.debug("form=" + form.data); 
            Redirect(routes.ProjectsController.newProject).flashing(Flash(form.data) + ("error" -> "Erreur de saisie. V��rifiez que tous les champs obligatoires sont saisis.") )
        },
        success = { newProject =>

          Projects.add(newProject)

          val message = "Ajout du client"
          Redirect(routes.ProjectsController.list(0,10)).flashing("success" -> message)
        }
      )
    }

  def edit(id: Long) = IsAuthenticated { implicit request =>
    val prj = models.Projects.findById(id)
    val customers = Customers.listAll

    val from = request.queryString.get("from").flatMap(_.headOption)
    Ok(views.html.project.editproject(prj.get,customers,from))
  }

  // POST
  def update(id: Long, from: String="/projects") = IsAuthenticated { implicit request =>
    val newProjectForm = this.projectForm.bindFromRequest()
    newProjectForm.fold(
      hasErrors = { form =>
        Redirect(routes.ProjectsController.edit(id)).
        flashing(Flash(form.data) +
        ("error" -> "Erreur de saisie. V��rifiez que tous les champs obligatoires sont saisis.") )
      },
      success = { newProject =>
        val p  = Projects.findById(id)
        models.Projects.update(id, p.get.idaddress, newProject)
        val message = "Project updated !"
        Redirect(from).flashing("success" -> message)
      }
    )
  }
  def delete(id: Long) = IsAuthenticated { implicit request =>
    Projects.delete(id)
    Redirect(routes.ProjectsController.list(0,10))
  }
}